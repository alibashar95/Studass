package modul1;

public class AverageSpeed {

    public static void main(String[] args) {

        double miles = 24.0;
        double time = 1.676;

        double averageMph = miles / time;
        System.out.println("Mph: " + averageMph);

        double averageKph = (miles * 1.6) / time;
        System.out.println("Kph: " + averageKph);

    }

}
