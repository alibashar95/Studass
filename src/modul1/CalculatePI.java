package modul1;

import java.util.Scanner;

public class CalculatePI {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of iterations: ");
        System.out.println(calculatePi(input.nextInt()));
        System.out.println("\nActual PI: \n" + Math.PI);

    }

    private static double calculatePi(int numberOfIterations) {

        double pi = 0;
        for (int i = 1; i <= numberOfIterations; i++) {
            pi += ((Math.pow(-1, i + 1) * 4) / (2 * i - 1));
        }
        return pi;

    }

}
