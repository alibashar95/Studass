package modul1;

public class Population {

    public static void main(String[] args) {

        long secondsInOneYear = 31557600;
        long currentPopulation = 3120324866L;
        long birthsInFiveYears = 5 * (secondsInOneYear / 7);
        long deathsInFiveYears = 5 * (secondsInOneYear / 13);
        long immigrantsInFiveYears = 5 * (secondsInOneYear / 45);

        long populationInFiveYears = currentPopulation + birthsInFiveYears + immigrantsInFiveYears - deathsInFiveYears;
        System.out.println("Population in five years: " + populationInFiveYears);

    }

}
