package modul2;

import java.util.Scanner;

public class SecondsConverter {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter number of seconds: ");
        int seconds = input.nextInt();
        int days = seconds / 86400;
        int hours = (seconds % 86400) / 3600;
        int minutes = (seconds % 3600) / 60;
        int secondsLeft = ((seconds % 86400 ) % 3600 ) % 60;

        System.out.println(seconds + " seconds = " + days + (days > 1 ? " days, " : " day, ") +
                           hours + (hours > 1 ? " hours, " : " hour, ") +
                           minutes + (minutes > 1 ? " minutes, and " : " minute, and ") +
                           secondsLeft + (secondsLeft > 1 ? " seconds." : " second"));

    }

}
